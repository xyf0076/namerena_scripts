const all = document.querySelector('#textdiv>textarea').value.split('\n');
let j = 0;
let fi = 0;
/**
 * Get the score of a name.
 */
function check() {
  if (fi) {
    return;
  }
  if (cw().document.querySelectorAll('span.u').length <= 10) {
    setTimeout(() => {
      check();
    }, 1000);
    return;
  }
  const val = parseInt(cw().document.querySelectorAll('span.u')[10].textContent
      .split(' ')[2]);
  if (val >= 6800) {
    console.log(`${all[j]} ${val}`);
  }
  j++;
  reload();
  setTimeout(() => {
    check();
  }, 1000);
}
/**
 * Reload names.
 */
function reload() {
  if (j < all.length) {
    document.querySelector('#textdiv>textarea').value = `!test!\n\n${all[j]}`;
    document.querySelector('.goBtn').click();
  } else {
    alert('测试已完成');
    fi = 1;
  }
}
reload();
check();
