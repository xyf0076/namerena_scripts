const all = document.querySelector('#textdiv>textarea').value.split('\n\n');
const cnt = {};
const fixedLen = all[0].split('\n').length;
const fixedNames = Array.prototype.slice.call(all[0].split('\n')).join('\n');
let randomNames = Array.prototype.slice.call(all[1].split('\n'));
while (randomNames[randomNames.length - 1] === '') {
  randomNames.pop();
}
randomNames.forEach((element) => {
  cnt[element] = 10;
});
randomNames = randomNames.concat(randomNames, randomNames, randomNames,
    randomNames, randomNames, randomNames, randomNames, randomNames, randomNames
);
const threshold = 6000;
const tot = 5;
const vis = new Set();
let namesBeingTested = '';
const outputName = Array.prototype.slice.call(all[0].split('\n')).join('+');
/**
 * Get the score of a name.
 */
function check() {
  if (cw().document.querySelectorAll('span.u').length <= 10) {
    setTimeout(() => {
      check();
    }, 1000);
    return;
  }
  const val = parseInt(cw().document.querySelectorAll('span.u')[10].textContent
      .split(' ')[2]);
  if (val >= threshold) {
    let tmp = outputName;
    for (const i of vis) {
      if (cnt[i] <= 1000) {
        for (let j = 0; j <= Math.floor((val - threshold) / 100); j++) {
          randomNames.push(i);
        }
        cnt[i] += Math.floor((val - threshold) / 100) + 1;
      }
      tmp += '+' + i;
    }
    document.querySelector('textarea#result').value += tmp + ' ' + val + '\n';
  }
  reload();
  setTimeout(() => {
    check();
  }, 1000);
}
/**
 * Reload names.
 */
function reload() {
  document.querySelector('#textdiv>textarea').value = '!test!\n!\n\n';
  namesBeingTested = fixedNames + '\n';
  vis.clear();
  for (let i = 0; i < tot - fixedLen; i++) {
    let name = randomNames[Math.floor(Math.random() * randomNames.length)];
    while (vis.has(name)) {
      name = randomNames[Math.floor(Math.random() * randomNames.length)];
    }
    namesBeingTested += name + '\n';
    vis.add(name);
  }
  document.querySelector('#textdiv>textarea').value += namesBeingTested;
  document.querySelector('.goBtn').click();
}
const NW = document.createElement('textarea');
NW.id = 'result';
document.body.appendChild(NW);
NW.setAttribute('readonly', true);
NW.setAttribute('style', 'width: 400px;');
document.getElementsByClassName('mdframe')[0].setAttribute('style',
    'display:none;');
reload();
check();
