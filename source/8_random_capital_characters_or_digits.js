const prefix = '';
const team = '';
let names = '';
let result = '';
/**
 * Get a random name.
 * @return {String} The name.
 */
function getRandomName() {
  let s = '';
  for (let i = 0; i < 8; i++) {
    const val = Math.floor(Math.random() * 36);
    if (val < 10) {
      s += String.fromCharCode(48 + val);
    } else {
      s += String.fromCharCode(55 + val);
    }
  }
  return s;
}
/**
 * Reload names.
 */
function reload() {
  names = team + '\n';
  for (let i = 0; i < 10; i++) {
    names += ` ${prefix}${getRandomName()}\n`;
  }
  document.querySelector('div>textarea').value = names;
  document.querySelector('.goBtn').click();
}
/**
 * Search for the names with elites.
 */
function maint() {
  if (cw().document.querySelectorAll('div.name').length < 10) {
    setTimeout(function() {
      maint();
    }, 100);
    return;
  }
  const a = cw().document.querySelectorAll('.s_elite3');
  for (const name of a) {
    result += name.parentElement.previousElementSibling.textContent.replace(
        /^\s+|\s+$/g, '') + '\n';
  }
  document.querySelector('textarea#result').value = result;
  reload();
  setTimeout(function() {
    maint();
  }, 100);
}
const NW = document.createElement('textarea');
NW.id = 'result';
document.body.appendChild(NW);
NW.setAttribute('readonly', true);
document.getElementsByClassName('mdframe')[0].setAttribute('style',
    'display:none;');
reload();
maint();
