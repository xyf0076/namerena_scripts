const prefix = '';
const team = '';
const nameLength = 5;
let names = '';
let name = '';
let result = '';
/**
 * Generate a random number.
 * @param {number} min min.
 * @param {number} max max.
 * @return {number} A random number in range [min, max]
 */
function randomAccess(min, max) {
  return Math.floor(min + Math.floor(Math.random() * (max - min + 1)));
}
/**
 * Get a random name.
 * @return {String} The name.
 */
function getRandomName() {
  name = '';
  for (let i = 0; i < nameLength; i++) {
    if (Math.floor(Math.random() * 2)) {
      name += String.fromCodePoint(randomAccess(0x2600, 0x27BF));
    } else {
      name += String.fromCodePoint(randomAccess(0x1F300, 0x1F64F));
    }
  }
  return name;
}
/**
 * Reload names.
 */
function reload() {
  names = team + '\n';
  for (let i = 0; i < 10; i++) {
    names += ` ${prefix}${getRandomName()}\n`;
  }
  document.querySelector('div>textarea').value = names;
  document.querySelector('.goBtn').click();
}
/**
 *  * Search for the names with elites.
 */
function maint() {
  if (cw().document.querySelectorAll('div.name').length < 10) {
    setTimeout(function() {
      maint();
    }, 100);
    return;
  }
  const a = cw().document.querySelectorAll('.s_elite3');
  for (const name of a) {
    result += name.parentElement.previousElementSibling.textContent.replace(
        /^\s+|\s+$/g, '') + '\n';
  }
  document.querySelector('textarea#result').value = result;
  reload();
  setTimeout(function() {
    maint();
  }, 100);
}
const NW = document.createElement('textarea');
NW.id = 'result';
document.body.appendChild(NW);
NW.setAttribute('readonly', true);
document.getElementsByClassName('mdframe')[0].setAttribute('style',
    'display:none;');
reload();
maint();
