const prefix = '';
const team = '';
const cur = 3;
let names = '';
let result = '';
/**
 * Generate a random number.
 * @param {number} min min.
 * @param {number} max max.
 * @return {number} A random number in range [min, max]
 */
function randomAccess(min, max) {
  return Math.floor(min + Math.floor(Math.random() * (max - min + 1)));
}
/**
 * Get a random name.
 * @param {number} cur The length of the name.
 * @return {String} The name.
 */
function getRandomName(cur) {
  let s = '';
  for (let i = 0; i < cur; i++) {
    s += String.fromCodePoint(randomAccess(0x4E00, 0x9FFF));
  }
  return s;
}
/**
 * Reload names.
 */
function reload() {
  names = team + '\n';
  for (let i = 0; i < 10; i++) {
    names += ` ${prefix}${getRandomName(cur)}\n`;
  }
  document.querySelector('div>textarea').value = names;
  document.querySelector('.goBtn').click();
}
/**
 * Search for the names with elites.
 */
function maint() {
  if (cw().document.querySelectorAll('div.name').length < 10) {
    setTimeout(function() {
      maint();
    }, 100);
    return;
  }
  const a = cw().document.querySelectorAll('.s_elite3');
  for (const name of a) {
    result += name.parentElement.previousElementSibling.textContent.replace(
        /^\s+|\s+$/g, '') + '\n';
  }
  document.querySelector('textarea#result').value = result;
  reload();
  setTimeout(function() {
    maint();
  }, 100);
}
const NW = document.createElement('textarea');
NW.id = 'result';
document.body.appendChild(NW);
NW.setAttribute('readonly', true);
document.getElementsByClassName('mdframe')[0].setAttribute('style',
    'display:none;');
reload();
maint();
