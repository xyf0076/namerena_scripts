// 遇到框框可以到这里查询:https://www.unicode.org/charts/unihan.html
const prefix = '';
const team = '';
const cur = 3;
let names = '';
let result = '';
/**
 * Generate a random number.
 * @param {number} min min.
 * @param {number} max max.
 * @return {number} A random number in range [min, max]
 */
function randomAccess(min, max) {
  return Math.floor(min + Math.floor(Math.random() * (max - min + 1)));
}
/**
 * Get a random name.
 * @param {number} cur The length of the name.
 * @return {String} The name.
 */
function getRandomName(cur) {
  let s = '';
  for (let i = 0; i < cur; i++) {
    const op = Math.floor(Math.random() * 7);
    switch (op) {
      case 0:
        s += String.fromCodePoint(randomAccess(0x4E00, 0x9FFF));
        break;
      case 1:
        s += String.fromCodePoint(randomAccess(0x3400, 0x4DBF));
        break;
      case 2:
        s += String.fromCodePoint(randomAccess(0x20000, 0x2A6DF));
        break;
      case 3:
        s += String.fromCodePoint(randomAccess(0x2A700, 0x2EBEF));
        break;
      case 4:
        s += String.fromCodePoint(randomAccess(0x30000, 0x3134F));
        break;
      case 5:
        s += String.fromCodePoint(randomAccess(0xF900, 0xFAFF));
        break;
      case 6:
        s += String.fromCodePoint(randomAccess(0x2F800, 0x2FA1F));
        break;
      default:
    }
  }
  return s;
}
/**
 * Reload names.
 */
function reload() {
  names = team + '\n';
  for (let i = 0; i < 10; i++) {
    names += ` ${prefix}${getRandomName(cur)}\n`;
  }
  document.querySelector('div>textarea').value = names;
  document.querySelector('.goBtn').click();
}
/**
 * Search for the names with elites.
 */
function maint() {
  if (cw().document.querySelectorAll('div.name').length < 10) {
    setTimeout(function() {
      maint();
    }, 100);
    return;
  }
  const a = cw().document.querySelectorAll('.s_elite3');
  for (const name of a) {
    result += name.parentElement.previousElementSibling.textContent.replace(
        /^\s+|\s+$/g, '') + '\n';
  }
  document.querySelector('textarea#result').value = result;
  reload();
  setTimeout(function() {
    maint();
  }, 100);
}
const NW = document.createElement('textarea');
NW.id = 'result';
document.body.appendChild(NW);
NW.setAttribute('readonly', true);
document.getElementsByClassName('mdframe')[0].setAttribute('style',
    'display:none;');
reload();
maint();
