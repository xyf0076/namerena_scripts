const all = document.querySelector('#textdiv>textarea').value.split('\n');
let i = 0;
let fi = 0;
let s = '';
/**
 * Get the score of a name.
 */
function check() {
  if (fi) {
    return;
  }
  if (cw().document.querySelectorAll('p').length <= 100) {
    setTimeout(() => {
      check();
    }, 1000);
    return;
  }
  const progress = cw().document.querySelectorAll('p');
  let p0 = -1;
  let pos = -1;
  for (let i = 100; i < progress.length; i++) {
    const element = progress[i];
    if (element.textContent === '实力评估中...100%') {
      p0 = i;
    }
    if (element.textContent === '实力评估中...101%') {
      pos = i;
      break;
    }
  }
  if (pos == -1) {
    setTimeout(() => {
      check();
    }, 1000);
    return;
  }
  s = progress[++p0].textContent + '\n';
  for (let i = p0 + 1; i < pos; i++) {
    s += progress[i].textContent + '\n';
  }
  console.log(s);
  i++;
  reload();
  setTimeout(() => {
    check();
  }, 10000);
}
/**
 * Reload.
 */
function reload() {
  if (i < all.length) {
    document.querySelector('#textdiv>textarea').value = '!test!\n\n' + all[i];
    document.querySelector('.goBtn').click();
  } else {
    alert('测试已完成');
    fi = 1;
  }
}
reload();
check();
