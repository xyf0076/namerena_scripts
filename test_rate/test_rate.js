let rivals;
let myName;
let res;
let NW;
let j = 0;
let fi = 0;
let ans = 0;
/**
 * Get the score of a name.
 */
function check() {
  if (fi) {
    return;
  }
  if (NW.contentWindow.cw().document.querySelectorAll('span.u').length <= 10) {
    setTimeout(() => {
      check();
    }, 1000);
    return;
  }
  const val = parseFloat(NW.contentWindow.cw().document.querySelectorAll(
      'span.u')[10].textContent.split(' ')[2]);
  res += `${rivals[j].replaceAll('\n', '+')}：${val}%\n`;
  ans += val;
  j++;
  reload();
  setTimeout(() => {
    check();
  }, 1000);
}
/**
 * Reload names.
 */
function reload() {
  document.querySelector('.result').value = `${res}\n平均：${ans / j}%\n`;
  if (j < rivals.length) {
    NW.contentWindow.document.querySelector('#textdiv>textarea').value =
      `!test!\n\n${myName}\n\n${rivals[j]}`;
    NW.contentWindow.document.querySelector('.goBtn').click();
  } else {
    setTimeout(() => {
      alert('测试已完成');
      fi = 1;
    }, 100);
  }
}
/**
 * Start.
 */
function start() {
  document.querySelector('#start').disabled = true;
  myName = document.querySelector('#myname').value;
  rivals = document.querySelector('#rivals').value.split('\n\n');
  res = myName.replaceAll('\n', '+') + ':\n';
  NW = document.createElement('iframe');
  NW.src = '../assets/index.html';
  NW.hidden = true;
  document.body.appendChild(NW);
  if (NW.attachEvent) {
    NW.attachEvent('onload', setTimeout(() => {
      reload();
      check();
    }, 5000));
  } else {
    NW.onload = setTimeout(() => {
      reload();
      check();
    }, 5000);
  }
}
